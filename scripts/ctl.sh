#!/bin/bash

CANVAS_DELAYED_SCRIPT="cd /opt/bitnami/apps/canvaslms/htdocs && RAILS_ENV=production /opt/bitnami/ruby/bin/ruby /opt/bitnami/apps/canvaslms/htdocs/script/delayed_job"

CANVAS_LOG=/opt/bitnami/apps/canvaslms/log/canvaslms.log
CANVAS_STATUS=""
ERROR=0

is_service_running() {
    if [ `id|sed -e s/uid=//g -e s/\(.*//g` -eq 0 ]; then
        OUTPUT=`su daemon -s /bin/sh -c "$CANVAS_DELAYED_SCRIPT status >> $CANVAS_LOG  2>&1"`
    else
        /bin/sh -c "$CANVAS_DELAYED_SCRIPT status >> $CANVAS_LOG 2>&1"
    fi
    ERRORCODE=$?
    DEAD=`echo $OUTPUT | grep 'No delayed' -c`

    if [ $ERRORCODE -eq 1 ] ; then
        RUNNING=0
    else
        if [ $DEAD -ne 0 ] ; then
            RUNNING=0
        else
            RUNNING=1
        fi
    fi

    return $RUNNING
}

is_canvas_running() {
    is_service_running
    RUNNING=$?
    if [ $RUNNING -eq 0 ]; then
        CANVAS_STATUS="canvas_init not running"
    else
        CANVAS_STATUS="canvas_init already running"
    fi
    return $RUNNING
}

start_canvas() {
    echo "Starting canvas_init daemon, it could take some seconds..."
    is_canvas_running
    RUNNING=$?

    if [ $DEAD -ne 0 ] ; then
        if [ `id|sed -e s/uid=//g -e s/\(.*//g` -eq 0 ]; then
            su daemon -s /bin/sh -c "$CANVAS_DELAYED_SCRIPT stop >> $CANVAS_LOG 2>&1"
        else
            /bin/sh -c "$CANVAS_DELAYED_SCRIPT stop >> $CANVAS_LOG 2>&1"
        fi
    fi


    if [ $RUNNING -eq 1  ]; then
        echo "$0 $ARG: canvas_init already running"
        exit
    else

        if [ `id|sed -e s/uid=//g -e s/\(.*//g` -eq 0 ]; then
            su daemon -s /bin/sh -c "$CANVAS_DELAYED_SCRIPT start >> $CANVAS_LOG 2>&1"
        else
            /bin/sh -c "$CANVAS_DELAYED_SCRIPT start >> $CANVAS_LOG 2>&1"
        fi
    fi
    sleep 1
    is_canvas_running
    RUNNING=$?
    if [ $RUNNING -eq 0 ]; then
        ERROR=1
    fi
    if [ $ERROR -eq 0 ]; then
        echo "$0 $ARG: canvas_init started"
    else
        echo "$0 $ARG: canvas_init could not be started"
        ERROR=3
    fi

}

stop_canvas() {
    echo "Stopping canvas_init daemon, it could take some seconds..."
    NO_EXIT_ON_ERROR=$1
    is_canvas_running
    RUNNING=$?
    if [ $RUNNING -eq 0 -o $DEAD -ne 0 ]; then
        echo "$0 $ARG: $CANVAS_STATUS"
        if [ "x$NO_EXIT_ON_ERROR" != "xno_exit" ]; then
            exit
        else
            return
        fi
    fi

    if [ `id|sed -e s/uid=//g -e s/\(.*//g` -eq 0 ]; then
        su daemon -s /bin/sh -c "$CANVAS_DELAYED_SCRIPT stop >> $CANVAS_LOG 2>&1"
    else
        /bin/sh -c "$CANVAS_DELAYED_SCRIPT stop >> $CANVAS_LOG 2>&1"
    fi

    is_canvas_running
    RUNNING=$?
    if [ $RUNNING -eq 0 ]; then
        echo "$0 $ARG: canvas_init stopped"
    else
        echo "$0 $ARG: canvas_init could not be stopped"
        ERROR=4
    fi
}

if [ "x$1" = "xstart" ]; then
    start_canvas
elif [ "x$1" = "xstop" ]; then
    stop_canvas
elif [ "x$1" = "xstatus" ]; then
    is_canvas_running
    echo "$CANVAS_STATUS"
fi

exit $ERROR
