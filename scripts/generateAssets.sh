#!/bin/sh
. /opt/bitnami/scripts/setenv.sh
cd /opt/bitnami/apps/canvaslms/htdocs
RAILS_ENV=production bundle exec rake brand_configs:generate_and_upload_all
                    